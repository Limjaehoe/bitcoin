package com.jhsoft.woghl.coin.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.jhsoft.woghl.coin.MainActivity
import com.jhsoft.woghl.coin.R
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() , GoogleApiClient.OnConnectionFailedListener{

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    private val TAG = "LoginActivity"
    private val RC_SIGN_IN = 9002
    private var mAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    var mGoogleApiClient: GoogleApiClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance();

        //파이어베이스 회원가입되었으면 자동으로 넘어가게..
        try {
            mAuth = FirebaseAuth.getInstance()
            mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
                println(firebaseAuth.currentUser)
                if (firebaseAuth.currentUser != null) {
                    //파이어베이스 인증되면 호출
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            mAuth!!.addAuthStateListener(mAuthListener!!) //로그인이 완료되면 호출됨..

        } catch (e: Exception) {
            e.printStackTrace()
        }


        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

        login_button.setOnClickListener {
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, RC_SIGN_IN)

        }

    }



    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                val account = result.getSignInAccount()

                val Token = result.signInAccount!!.idToken
                val credential = GoogleAuthProvider.getCredential(Token, null)
                mAuth!!.signInWithCredential(credential)
                        .addOnCompleteListener(this) { task ->
                            Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful)
                            task.result.user.getToken(true).addOnCompleteListener { task ->
                                Log.d(TAG, "FireBase:" + task.result.token!!.toString())
                                //MySharedPreferences.setSharedPreferences("FireBaseToken", task.result.token!!.toString())
                            }
                            if (!task.isSuccessful) {
                                Log.w(TAG, "signInWithCredential", task.exception)
                                Toast.makeText(applicationContext, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show()
                            }
                        }
                Log.d("GoogleSignInApi", result.isSuccess.toString())
                if(result.isSuccess()){
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(intent)
                }
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
    }


}
