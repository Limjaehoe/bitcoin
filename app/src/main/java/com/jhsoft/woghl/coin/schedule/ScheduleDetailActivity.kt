package com.jhsoft.woghl.coin.schedule

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.jhsoft.woghl.coin.R
import com.jhsoft.woghl.coin.model.ScheduleDTO
import com.jhsoft.woghl.coin.restapi.RestApiCoin
import com.jhsoft.woghl.coin.restapi.ServerGenerator
import kotlinx.android.synthetic.main.activity_schedule_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class ScheduleDetailActivity : AppCompatActivity() {
    var scheduleresponse : ArrayList<ScheduleDTO>? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_detail)

        var scheduledto = intent.getParcelableExtra<ScheduleDTO>("schedule")

        //like_text.text  = scheduledto.ds_like.toString()
        //unlike_text.text = scheduledto.ds_unlike.toString()
        toptitle_date.text = scheduledto.dt_write.toString().replace("-","/") + " ("+getDateDay(scheduledto.dt_write.toString(),"yyyy-MM-dd") +"요일)"
        toptitle_name.text = scheduledto.ds_name.toString()
        toptitle_content.text = scheduledto.ds_content.toString()
        vote_sum.text  = "투표인원: "+ (Integer.valueOf(scheduledto.ds_like.toString())+Integer.valueOf(scheduledto.ds_unlike.toString())).toString() + "명"
        contentdetail.text = scheduledto.ds_contentdetail.toString()

        contentlink.setOnClickListener {
            try {
                val urltemp = scheduledto.ds_link.toString()
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urltemp))
                startActivity(intent)
            } catch (e: Exception) {
                Toast.makeText(applicationContext, "링크가없습니다", Toast.LENGTH_SHORT).show()
            }
        }

        try {
            var likenum = scheduledto.ds_like.toString();
            var unlikenum = scheduledto.ds_unlike.toString();

            val valuesum = Integer.valueOf(likenum)+Integer.valueOf(unlikenum)
            val value = division(Integer.valueOf(likenum),Integer.valueOf(valuesum)) //이것만안됨..

            vote_rate.text = "비율: "+ String.format("%.1f",value) + "%"//비율

                    // 문자열을 숫자로 변환.
            if (value < 0 || value > 100) {
                val toast = Toast.makeText(applicationContext, "0 to 100 can be input.",
                        Toast.LENGTH_SHORT)
                toast.show()
            } else {
                // 변환된 값을 프로그레스바에 적용.
                val progress = progressBar as ProgressBar
                progress.progress = value.toInt()
            }
        } catch (e: Exception) {
            // 토스트(Toast) 메시지 표시.
            val toast = Toast.makeText(applicationContext, "Invalid number format",
                    Toast.LENGTH_SHORT)
            toast.show()
        }


        //회원아이디보내기 dt_write 대신에;
        like_button.setOnClickListener {
            val retrofit = ServerGenerator.getfinfssServer()
            val restApifinfss = retrofit.create(RestApiCoin::class.java!!)
            val depositDTOCall = restApifinfss.requestselectcoinschedulelike(scheduledto.sn_seq.toString(), FirebaseAuth.getInstance().currentUser!!.email!!)
            depositDTOCall.enqueue(object : Callback<ArrayList<ScheduleDTO>> {

                override fun onFailure(call: Call<ArrayList<ScheduleDTO>>?, t: Throwable?) {

                }

                override fun onResponse(call: Call<ArrayList<ScheduleDTO>>?, response: Response<ArrayList<ScheduleDTO>>?) {
                    scheduleresponse = response!!.body()

                    var printtoask = scheduleresponse!!.get(0).ds_recommend_confirm.toString()
                    if(printtoask.equals("1"))
                    {
                        Toast.makeText(applicationContext, "추천되었습니다.", Toast.LENGTH_LONG).show()
                    }
                    else{
                        Toast.makeText(applicationContext, "이미 추천을 했습니다.", Toast.LENGTH_LONG).show()
                    }

                    try {
                        var likenum = scheduleresponse!!.get(0).ds_like.toString();
                        var unlikenum = scheduleresponse!!.get(0).ds_unlike.toString();

                        val valuesum = Integer.valueOf(likenum)+Integer.valueOf(unlikenum)
                        val value = division(Integer.valueOf(likenum),Integer.valueOf(valuesum)) //이것만안됨..
                        vote_sum.text  = "투표인원: "+ valuesum.toString() + "명"
                        vote_rate.text = "비율: "+ String.format("%.1f",value) + "%"//비율

                        // 문자열을 숫자로 변환.
                        if (value < 0 || value > 100) {
                            val toast = Toast.makeText(applicationContext, "0 to 100 can be input.",
                                    Toast.LENGTH_SHORT)
                            toast.show()
                        } else {
                            // 변환된 값을 프로그레스바에 적용.
                            val progress = progressBar as ProgressBar
                            progress.progress = value.toInt()
                        }
                    } catch (e: Exception) {
                        // 토스트(Toast) 메시지 표시.
                        val toast = Toast.makeText(applicationContext, "Invalid number format",
                                Toast.LENGTH_SHORT)
                        toast.show()
                    }

                }
            })
        }



        unlike_button.setOnClickListener {
            val retrofit = ServerGenerator.getfinfssServer()
            val restApifinfss = retrofit.create(RestApiCoin::class.java!!)
            val depositDTOCall = restApifinfss.requestselectcoinscheduleunlike(scheduledto.sn_seq.toString(), FirebaseAuth.getInstance().currentUser!!.email!!)
            depositDTOCall.enqueue(object : Callback<ArrayList<ScheduleDTO>> {
                override fun onFailure(call: Call<ArrayList<ScheduleDTO>>?, t: Throwable?) {

                }

                override fun onResponse(call: Call<ArrayList<ScheduleDTO>>?, response: Response<ArrayList<ScheduleDTO>>?) {
                    scheduleresponse = response!!.body()

                    var printtoask = scheduleresponse!!.get(0).ds_recommend_confirm.toString()
                    if(printtoask.equals("1"))
                    {
                        Toast.makeText(applicationContext, "추천되었습니다.", Toast.LENGTH_LONG).show()
                    }
                    else{
                        Toast.makeText(applicationContext, "이미 추천을 했습니다.", Toast.LENGTH_LONG).show()
                    }

                    try {
                        var likenum = scheduleresponse!!.get(0).ds_like.toString();
                        var unlikenum = scheduleresponse!!.get(0).ds_unlike.toString();

                        val valuesum = Integer.valueOf(likenum)+Integer.valueOf(unlikenum)
                        val value = division(Integer.valueOf(likenum),Integer.valueOf(valuesum)) //이것만안됨..
                        vote_sum.text  = "투표인원: "+ valuesum.toString() + "명"
                        vote_rate.text = "비율: "+ String.format("%.1f",value) + "%"//비율


                        // 문자열을 숫자로 변환.
                        if (value < 0 || value > 100) {
                            val toast = Toast.makeText(applicationContext, "0 to 100 can be input.",
                                    Toast.LENGTH_SHORT)
                            toast.show()
                        } else {
                            // 변환된 값을 프로그레스바에 적용.
                            val progress = progressBar as ProgressBar
                            progress.progress = value.toInt()
                        }
                    } catch (e: Exception) {
                        // 토스트(Toast) 메시지 표시.
                        val toast = Toast.makeText(applicationContext, "Invalid number format",
                                Toast.LENGTH_SHORT)
                        toast.show()
                    }





                }
            })


        }

    }

    fun division(a: Int, b: Int): Float {
        return (a.toFloat() / b.toFloat()) * 100
    }
    fun sum(a: Int, b: Int): Int {
        return a + b
    }

    @Throws(Exception::class)
    fun getDateDay(date: String, dateType: String): String {

        var day = ""

        val dateFormat = SimpleDateFormat(dateType)
        val nDate = dateFormat.parse(date)

        val cal = Calendar.getInstance()
        cal.setTime(nDate)

        val dayNum = cal.get(Calendar.DAY_OF_WEEK)

        when (dayNum) {
            1 -> day = "일"
            2 -> day = "월"
            3 -> day = "화"
            4 -> day = "수"
            5 -> day = "목"
            6 -> day = "금"
            7 -> day = "토"
        }

        return day
    }
}
