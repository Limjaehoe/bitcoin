package com.jhsoft.woghl.coin.utl

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import org.json.JSONObject
import org.jsoup.Jsoup

/**
 * Created by Lim on 2016-12-20.
 */

class VersionChecker(private val context: Context) {
    //최신버전체크!
    private val currentVersion: String
    private var latestVersion: String? = null
    private var dialog: Dialog? = null

    init {
        val pm = context.packageManager
        var pInfo: PackageInfo? = null

        try {
            pInfo = pm.getPackageInfo(context.packageName, 0)

        } catch (e1: PackageManager.NameNotFoundException) {
            // TODO Auto-generated catch block
            e1.printStackTrace()
        }

        currentVersion = pInfo!!.versionName

        //System.out.println(currentVersion);
        GetLatestVersion().execute()

    }

    private inner class GetLatestVersion : AsyncTask<String, String, JSONObject>() {

        private val progressDialog: ProgressDialog? = null

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: String): JSONObject {
            try {
                //It retrieves the latest version by scraping the content of current version from play store at runtime
                val doc = Jsoup.connect("https://play.google.com/store/apps/details?id=com.jhsoft.woghl.coin").get()
                latestVersion = doc.getElementsByAttributeValue("itemprop", "softwareVersion").first().text()

            } catch (e: Exception) {
                e.printStackTrace()
            }

            return JSONObject()
        }

        override fun onPostExecute(jsonObject: JSONObject) {
            if (latestVersion != null) {
                if (!currentVersion.equals(latestVersion!!, ignoreCase = true)) {
                    showUpdateDialog()
                }
            } else
            //background.start();
                super.onPostExecute(jsonObject)
        }
    }

    //여기다가 Fragment 만들어서 호출하면된다..!!
    private fun showUpdateDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("앱이 업데이트 되었습니다. 연결을 누르면 업데이트화면으로 이동합니다.")
        builder.setPositiveButton("연결") { dialog, which ->
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.jhsoft.woghl.coin")))
            dialog.dismiss()
        }

        //캔슬
        builder.setNegativeButton("닫기") { dialog, which ->
            //background.start();
            //finish();
        }

        builder.setCancelable(false)
        dialog = builder.show()
    }

}
