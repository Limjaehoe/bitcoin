package com.jhsoft.woghl.coin.model

/**
 * Created by woghl on 2018-02-08.
 */

data class RankDTO (
        var rank : String?,
        var ds_name : String?,
        var cnt : String?
)