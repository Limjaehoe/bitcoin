package com.jhsoft.woghl.coin.restapi

import com.jhsoft.woghl.coin.model.G5_BoardDTO
import com.jhsoft.woghl.coin.model.RankDTO
import com.jhsoft.woghl.coin.model.ScheduleDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part


interface RestApiCoin {


    @GET("coin_schedule/select_coin_schedule.php")
    abstract fun requestselectcoinschedule(): Call<Map<String,List<ScheduleDTO>>>

    @GET("coin_schedule/select_coin_rank.php")
    abstract fun requestselectcoinrank(): Call<List<RankDTO>>

    @GET("coin_schedule/select_coin_rank20.php")
    abstract fun requestselectcoinrank20(): Call<List<RankDTO>>

    @GET("g5_board/write_board.php")
    abstract fun requestg5board(): Call<ArrayList<G5_BoardDTO>>


    @Multipart
    @POST("coin_schedule/select_coin_schedule_like.php")
    fun requestselectcoinschedulelike(
            @Part("sn_seq") sn_seq: String,
            @Part("dt_write") dt_write: String): Call<ArrayList<ScheduleDTO>>

    @Multipart
    @POST("coin_schedule/select_coin_schedule_unlike.php")
    fun requestselectcoinscheduleunlike(
            @Part("sn_seq") sn_seq: String,
            @Part("dt_write") dt_write: String): Call<ArrayList<ScheduleDTO>>

    @Multipart
    @POST("coin_schedule/select_coin_search.php")
    fun requestselectcoinschedulesearch(
            @Part("ds_name") ds_name: String): Call<Map<String,List<ScheduleDTO>>>




    /*@Multipart
    @POST("member/select_coin_schedule.php")
    fun requestselectcoinschedule(
            @Part("plan") plan: String,
            @Part("gubun") gubun: String,
            @Part("sex") sex: String,
            @Part("old") old: String,
            @Part("old2") old2: String,
            @Part("bohum") bohum: String): Call<ArrayList<ScheduleDTO>>*/


}