package com.jhsoft.woghl.coin.button2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jhsoft.woghl.coin.R

/**
 * Created by woghl on 2018-02-02.
 */
class Button2Fragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater!!.inflate(R.layout.button2_fragment,container,false)


        var recyclerView = view.findViewById<RecyclerView>(R.id.rankrecyclerview)
        recyclerView.adapter = RankAdapter()
        recyclerView.layoutManager = LinearLayoutManager(inflater.context)

        var recyclerView20 = view.findViewById<RecyclerView>(R.id.rankrecyclerview20)
        recyclerView20.adapter = RankAdapter20()
        recyclerView20.layoutManager = LinearLayoutManager(inflater.context)

        return view
    }
}