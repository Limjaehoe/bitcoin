package com.jhsoft.woghl.coin.button3

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jhsoft.woghl.coin.R
import com.jhsoft.woghl.coin.model.G5_BoardDTO
import com.jhsoft.woghl.coin.restapi.RestApiCoin
import com.jhsoft.woghl.coin.restapi.ServerGenerator
import kotlinx.android.synthetic.main.button3_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by woghl on 2018-02-02.
 */
class Button3Fragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater!!.inflate(R.layout.button3_fragment,container,false)

        val retrofit = ServerGenerator.getfinfssServer() //서버주소받오기
        val restApiPowerfc = retrofit.create(RestApiCoin::class.java!!) //restapi에서 interface정의해온거 받아오기
        val g5boardDTOCall = restApiPowerfc.requestg5board() // 호출해주기
        g5boardDTOCall.enqueue(object : Callback<ArrayList<G5_BoardDTO>> {
            override fun onResponse(call: Call<ArrayList<G5_BoardDTO>>, response: Response<ArrayList<G5_BoardDTO>>) {

                //recyclerView = findViewById(R.id.g5board_recyclerlistview) as RecyclerView

                var g5_boardRecyclerViewAdapter = G5_BoardRecyclerViewAdapter(response.body())
                val layoutManager = LinearLayoutManager(view.context)

                g5_recyclerview.setLayoutManager(layoutManager)
                g5_recyclerview.setAdapter(g5_boardRecyclerViewAdapter)

            }

            override fun onFailure(call: Call<ArrayList<G5_BoardDTO>>, t: Throwable) {

            }
        })

        return view
    }
}