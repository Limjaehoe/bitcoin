package com.jhsoft.woghl.coin.model
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by woghl on 2018-01-28.
 */

data class ScheduleDTO (
        var sn_seq: String?,
        var ds_name: String?,
        var ds_ename: String?,
        var ds_content: String?,
        var ds_contentdetail: String?,
        var ds_link : String?,
        var dt_write: String?,
        var ds_like : String?,
        var ds_unlike : String?,
        var ds_recommend_confirm : String?

)  : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sn_seq)
        parcel.writeString(ds_name)
        parcel.writeString(ds_ename)
        parcel.writeString(ds_content)
        parcel.writeString(ds_contentdetail)
        parcel.writeString(ds_link)
        parcel.writeString(dt_write)
        parcel.writeString(ds_like)
        parcel.writeString(ds_unlike)
        parcel.writeString(ds_recommend_confirm)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ScheduleDTO> {
        override fun createFromParcel(parcel: Parcel): ScheduleDTO {
            return ScheduleDTO(parcel)
        }

        override fun newArray(size: Int): Array<ScheduleDTO?> {
            return arrayOfNulls(size)
        }
    }

}
