package com.jhsoft.woghl.coin.button2

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jhsoft.woghl.coin.R
import com.jhsoft.woghl.coin.model.RankDTO
import com.jhsoft.woghl.coin.restapi.RestApiCoin
import com.jhsoft.woghl.coin.restapi.ServerGenerator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by woghl on 2018-02-08.
 */

class RankAdapter20 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var rankdto : List<RankDTO>? = null

    init {
        val retrofit = ServerGenerator.getfinfssServer()
        val restApifinfss = retrofit.create(RestApiCoin::class.java!!)
        val rankDTOCall = restApifinfss.requestselectcoinrank20()
        rankDTOCall.enqueue(object : Callback<List<RankDTO>> {
            override fun onFailure(call: Call<List<RankDTO>>?, t: Throwable?) {
                println("fail")
            }
            override fun onResponse(call: Call<List<RankDTO>>?, response: Response<List<RankDTO>>?) {

                rankdto = response!!.body()

                notifyDataSetChanged()
            }
        })


    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.rank_recyclerview_item, parent, false)
        return RankAdapter.RankViewHolder(view)
    }

    override fun getItemCount(): Int {
        if(rankdto==null)
        {
            return 0
        }

        return rankdto!!.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        var view = holder as RankAdapter.RankViewHolder
        view.rank!!.text = rankdto!!.get(position).rank
        view.name!!.text = rankdto!!.get(position).ds_name
        view.cnt!!.text = rankdto!!.get(position).cnt
    }

    class RankViewHolder(view: View?) : RecyclerView.ViewHolder(view){
        var rank : TextView? = null
        var name : TextView? = null
        var cnt : TextView? = null
        init {
            rank = view!!.findViewById<TextView>(R.id.rank_1)
            name = view!!.findViewById<TextView>(R.id.rank_2)
            cnt = view!!.findViewById<TextView>(R.id.rank_3)
        }
    }


}