package com.jhsoft.woghl.coin.button3

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.TextView
import com.jhsoft.woghl.coin.R
import com.jhsoft.woghl.coin.model.G5_BoardDTO
import java.util.*

/**
 * Created by Lim on 2017-06-17.
 */

class G5_BoardRecyclerViewAdapter(private val listdata: ArrayList<G5_BoardDTO>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mCustomView: View? = null
    private val mWebChromeClient: myWebChromeClient? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_g5_board, parent, false)
        return RowViewHolder(itemView)
    }

    fun inCustomView(): Boolean {
        return mCustomView != null
    }

    fun hideCustomView() {
        //myWebChromeClient.
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RowViewHolder).g5_subject.setText(listdata[position].wr_subject)
        holder.g5_datetime.setText(listdata[position].wr_datetime!!.substring(0, 10))
        holder.webView.visibility = View.GONE
        holder.webView.setBackgroundColor(0)

        try {
            val data = listdata[position].wr_content!!.split("<iframe")
            holder.g5_content.setText(data[0])

            val data_html = "<iframe " + data[1] //listdata.get(position).wr_link;
            holder.webView.settings.javaScriptEnabled = true
            holder.webView.loadData(data_html, "text/html", "UTF-8")

            holder.webView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    view.visibility = View.VISIBLE
                    super.onPageFinished(view, url)
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    return super.shouldOverrideUrlLoading(view, url)
                }
            }

            val mWebChromeClient = myWebChromeClient()
            holder.webView.webChromeClient = mWebChromeClient
            holder.webView.settings.setAppCacheEnabled(true)
            holder.webView.settings.builtInZoomControls = true
            holder.webView.settings.saveFormData = true

        } catch (e: Exception) {
            e.printStackTrace()
        }

        holder.g5_content_linearlayout.visibility = View.GONE //goneeee
        holder.itemView.setOnClickListener {
            if (holder.g5_content_linearlayout.visibility == View.VISIBLE) {
                holder.g5_content_linearlayout.visibility = View.GONE
            } else {
                holder.g5_content_linearlayout.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemCount(): Int {
        return listdata.size
    }

    private inner class RowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var g5_subject: TextView
        var g5_content: TextView
        var g5_datetime: TextView
        var g5_content_linearlayout: LinearLayout
        var webView: WebView

        init {
            g5_subject = itemView.findViewById(R.id.item_g5_subject)
            g5_content = itemView.findViewById(R.id.item_g5_content)
            g5_content_linearlayout = itemView.findViewById(R.id.item_g5_content_linearlayout)
            g5_datetime = itemView.findViewById(R.id.item_g5_datetime)
            webView = itemView.findViewById(R.id.g5_WebViewActivity)
        }
    }


    internal inner class myWebChromeClient : WebChromeClient() {
        private var mCustomView: View? = null
        private var customViewCallback: WebChromeClient.CustomViewCallback? = null

        override fun onShowCustomView(view: View, requestedOrientation: Int, callback: WebChromeClient.CustomViewCallback) {
            onShowCustomView(view, callback)    //To change body of overridden methods use File | Settings | File Templates.
        }

        override fun onShowCustomView(view: View, callback: WebChromeClient.CustomViewCallback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden()
                return
            }
            mCustomView = view
            //linearLayout.visibility = View.GONE
            //customViewContainer.visibility = View.VISIBLE
            //customViewContainer.addView(view)
            customViewCallback = callback
        }

        override fun onHideCustomView() {
            super.onHideCustomView()    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return

            //linearLayout.visibility = View.VISIBLE
            //customViewContainer.visibility = View.GONE

            // Hide the custom view.
            mCustomView!!.visibility = View.GONE

            // Remove the custom view from its container.

            //customViewContainer.removeView(mCustomView)
            customViewCallback!!.onCustomViewHidden()

            mCustomView = null
        }
    }

    internal inner class myWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            return super.shouldOverrideUrlLoading(view, url)
        }
    }

}
