package com.jhsoft.woghl.coin.restapi

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by Lim on 2017-01-19.
 */

object ServerGenerator {
    fun getfinfssServer(): Retrofit {
        val builder = Retrofit.Builder().baseUrl(ServerValue.finfssServerUrl).addConverterFactory(ToStringConverterFactory()).addConverterFactory(GsonConverterFactory.create())
        return builder.build()
    }
}
