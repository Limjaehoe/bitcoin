package com.jhsoft.woghl.coin.button4

import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.jhsoft.woghl.coin.R
import kotlinx.android.synthetic.main.activity_donation.*



class DonationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_donation)


        donation_btc.setOnClickListener {

            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboardManager!!.setText("37CS8r24zVsaVLxSx5a8itzLWLdezyTzwC")

            Toast.makeText(applicationContext, "BTC지갑 주소가 복사되었습니다. 37CS8r24zVsaVLxSx5a8itzLWLdezyTzwC ", Toast.LENGTH_LONG).show()

        }

        donation_eth.setOnClickListener {
            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboardManager!!.setText("0x43d455ca16fc59b28e5efff6a4d40c0157cf3468")

            Toast.makeText(applicationContext, "ETC지갑 주소가 복사되었습니다. 0x43d455ca16fc59b28e5efff6a4d40c0157cf3468", Toast.LENGTH_LONG).show()

        }

        donation_etc.setOnClickListener {
            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboardManager!!.setText("0xb10dbb76441f50149db8cf3261b6a8b4fb235fee")

            Toast.makeText(applicationContext, "ETC지갑 주소가 복사되었습니다. 0xb10dbb76441f50149db8cf3261b6a8b4fb235fee", Toast.LENGTH_LONG).show()

        }

        donation_ltc.setOnClickListener {
            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboardManager!!.setText("3MQuLFdaok9sAfexWLVxZodYkR2hS1ijAW")

            Toast.makeText(applicationContext, "ETC지갑 주소가 복사되었습니다. 3MQuLFdaok9sAfexWLVxZodYkR2hS1ijAW", Toast.LENGTH_LONG).show()


        }


    }





}
