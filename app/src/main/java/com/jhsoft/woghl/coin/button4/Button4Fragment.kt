package com.jhsoft.woghl.coin.button4

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.jhsoft.woghl.coin.R



/**
 * Created by woghl on 2018-02-02.
 */

class Button4Fragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater!!.inflate(R.layout.button4_fragment,container,false)

        val donation = view.findViewById<Button>(R.id.fragment4_button1)
        donation.setOnClickListener { view ->
            val intent = Intent(view.context, DonationActivity::class.java)
            startActivity(intent)
        }


        val button2 = view.findViewById<Button>(R.id.fragment4_button2)
        button2.setOnClickListener {
            val email = Intent(Intent.ACTION_SEND)
            email.type = "plain/text"
            // email setting 배열로 해놔서 복수 발송 가능
            val address = arrayOf("woghl1129@gmail.com")
            email.putExtra(Intent.EXTRA_EMAIL, address)
            email.putExtra(Intent.EXTRA_SUBJECT, "코인캘린더 문의하기")
            email.putExtra(Intent.EXTRA_TEXT, "문의내용: .\n")
            startActivity(email)
        }


        val button3 = view.findViewById<Button>(R.id.fragment4_button3)
        button3.setOnClickListener {
            val email = Intent(Intent.ACTION_SEND)
            email.type = "plain/text"
            // email setting 배열로 해놔서 복수 발송 가능
            val address = arrayOf("woghl1129@gmail.com")
            email.putExtra(Intent.EXTRA_EMAIL, address)
            email.putExtra(Intent.EXTRA_SUBJECT, "코인캘린더 일정신고")
            email.putExtra(Intent.EXTRA_TEXT, "문의내용: .\n")
            startActivity(email)

        }
        return view
    }
}