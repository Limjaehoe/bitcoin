package com.jhsoft.woghl.coin.schedule

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jhsoft.woghl.coin.R
import com.jhsoft.woghl.coin.model.ScheduleDTO
import com.jhsoft.woghl.coin.restapi.RestApiCoin
import com.jhsoft.woghl.coin.restapi.ServerGenerator
import com.jhsoft.woghl.coin.utl.CoinImageMapper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by woghl on 2018-01-28.
 */

class ScheduleAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>{

    var scheduledto : Map<String,List<ScheduleDTO>>? = null
    var keyArray: ArrayList<String>? = null //날짜
    var valueArray : ArrayList<ScheduleDTO>? = ArrayList() // 날짜 + value안에있는 content들

     init{

     }


    constructor() {
        //통신
        val retrofit = ServerGenerator.getfinfssServer()
        val restApifinfss = retrofit.create(RestApiCoin::class.java!!)
        val depositDTOCall = restApifinfss.requestselectcoinschedule()
        depositDTOCall.enqueue(object : Callback<Map<String,List<ScheduleDTO>>> {
            override fun onFailure(call: Call<Map<String, List<ScheduleDTO>>>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Map<String, List<ScheduleDTO>>>?, response: Response<Map<String, List<ScheduleDTO>>>?) {
                scheduledto = response!!.body();


                keyArray = ArrayList(scheduledto!!.keys) //키값
                for ((index,date) in keyArray!!.withIndex()) { //  withindex를 쓰면 index값까지 불러드린다
                    //헤더만듬
                    var scheduleDTO = ScheduleDTO(null, null, null, null, null,null, keyArray!![index]!!, null, null, null)
                    valueArray!!.add(scheduleDTO) //head연결 (date)

                    //tail연결 (content)
                    for (value in scheduledto!![date]!!) {
                        //println(value.ds_name)
                        valueArray!!.add(value)
                    }
                }
                notifyDataSetChanged()
                println(valueArray.toString())
            }

        })

    }
    constructor(name: String){
        if(name.length==0)
        {
            val retrofit = ServerGenerator.getfinfssServer()
            val restApifinfss = retrofit.create(RestApiCoin::class.java!!)
            val depositDTOCall = restApifinfss.requestselectcoinschedule()
            depositDTOCall.enqueue(object : Callback<Map<String,List<ScheduleDTO>>> {
                override fun onFailure(call: Call<Map<String, List<ScheduleDTO>>>?, t: Throwable?) {

                }

                override fun onResponse(call: Call<Map<String, List<ScheduleDTO>>>?, response: Response<Map<String, List<ScheduleDTO>>>?) {
                    scheduledto = response!!.body()
                    keyArray = ArrayList(scheduledto!!.keys) //키값
                    for ((index,date) in keyArray!!.withIndex()) { //  withindex를 쓰면 index값까지 불러드린다
                        //헤더만듬
                        var scheduleDTO = ScheduleDTO(null, null, null, null, null,null, keyArray!![index]!!, null, null, null)
                        valueArray!!.add(scheduleDTO) //head연결 (date)
                        //tail연결 (content)
                        for (value in scheduledto!![date]!!) {
                            //println(value.ds_name)
                            valueArray!!.add(value)
                        }
                    }
                    notifyDataSetChanged()
                    println(valueArray.toString())
                }

            })
        }
        else
        {
            val retrofit = ServerGenerator.getfinfssServer()
            val restApifinfss = retrofit.create(RestApiCoin::class.java!!)
            val depositDTOCall = restApifinfss.requestselectcoinschedulesearch(name)
            depositDTOCall.enqueue(object : Callback<Map<String, List<ScheduleDTO>>> {
                override fun onFailure(call: Call<Map<String, List<ScheduleDTO>>>?, t: Throwable?) {

                }

                override fun onResponse(call: Call<Map<String, List<ScheduleDTO>>>?, response: Response<Map<String, List<ScheduleDTO>>>?) {

                    scheduledto = response!!.body();
                    if(scheduledto != null){
                        println(scheduledto!!.keys)
                        keyArray = ArrayList(scheduledto!!.keys) //키값
                        for ((index,date) in keyArray!!.withIndex()) { //  withindex를 쓰면 index값까지 불러드린다

                            //헤더만듬
                            var scheduleDTO = ScheduleDTO(null, null, null, null, null,null, keyArray!![index]!!, null, null, null)
                            valueArray!!.add(scheduleDTO) //head연결 (date)

                            //tail연결 (content)
                            for (value in scheduledto!![date]!!) {
                                println(value.ds_name)
                                valueArray!!.add(value)
                            }
                        }
                        notifyDataSetChanged()
                        println(valueArray.toString())

                    }
                }


            })

        }



    }


    override fun getItemViewType(position: Int): Int {
        //이름값이 null일때 헤더값인지 구분
        if (valueArray!![position].ds_name == null){
            return 0
        }

        //이름값이 있을땐 헤더가아닌 content
        return 1
    }

    override fun getItemCount(): Int {

        if(scheduledto==null)
        {
            return 0
        }

        return valueArray!!.size

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {

        //헤더
        if(viewType == 0){
           var view = LayoutInflater.from(parent!!.context).inflate(R.layout.schedule_recyclerview_header, parent,false)
           return HeaderViewHolder(view)
        }

        //content
        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.schedule_recyclerview_item, parent,false)
        return CustomViewHolder1(view)

    }

    class HeaderViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
        var date :TextView? = null
        var week :TextView? = null
        init {
            date = view!!.findViewById<TextView>(R.id.schedule_date)
            week = view!!.findViewById<TextView>(R.id.schedule_week)
        }
    }

    class CustomViewHolder1(view: View?) : RecyclerView.ViewHolder(view) {
        var name :TextView? = null
        var content :TextView? = null
        var image : ImageView? = null

        init {
            name = view!!.findViewById<TextView>(R.id.schedule_name)
            content = view!!.findViewById<TextView>(R.id.schedule_content)
            image = view!!.findViewById(R.id.schedule_image)
        }

    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        //getItemViewType <-60째줄 펑션
        //head
        if(getItemViewType(position) == 0){
            var view = holder as HeaderViewHolder
            view.date!!.text = valueArray!!.get(position)!!.dt_write
            view.week!!.text = "("+getDateDay(valueArray!!.get(position)!!.dt_write!!,"yyyy-MM-dd") +"요일)"
        }else{//tail content
            var view = holder as CustomViewHolder1
            view.name!!.text = valueArray!!.get(position)!!.ds_name +" ("+ valueArray!!.get(position)!!.ds_ename +")"
            view.content!!.text = valueArray!!.get(position)!!.ds_content

            //val imagereturn = CoinImageMapper().getCoinImageMapper("EOIS")

            val imagereturn = CoinImageMapper().getCoinImageMapper(valueArray!!.get(position)!!.ds_ename.toString())
            Glide.with(holder.itemView.context)
                    .load(imagereturn)
                    .into(view.image)
            //view.image!!.setImageResource(imagereturn)

            view.itemView.setOnClickListener {
                var intent = Intent(view.itemView.context, ScheduleDetailActivity::class.java)
                intent.putExtra("schedule", valueArray!!.get(position) )
                view.itemView.context.startActivity(intent)
            }

        }
    }

    @Throws(Exception::class)
    fun getDateDay(date: String, dateType: String): String {

        var day = ""
        val dateFormat = SimpleDateFormat(dateType)
        val nDate = dateFormat.parse(date)
        val cal = Calendar.getInstance()
        cal.setTime(nDate)
        val dayNum = cal.get(Calendar.DAY_OF_WEEK)

        when (dayNum) {
            1 -> day = "일"
            2 -> day = "월"
            3 -> day = "화"
            4 -> day = "수"
            5 -> day = "목"
            6 -> day = "금"
            7 -> day = "토"
        }

        return day
    }


    fun test(){
        val retrofit = ServerGenerator.getfinfssServer()
        val restApifinfss = retrofit.create(RestApiCoin::class.java!!)
        val depositDTOCall = restApifinfss.requestselectcoinschedule()
        depositDTOCall.enqueue(object : Callback<Map<String,List<ScheduleDTO>>> {
            override fun onFailure(call: Call<Map<String, List<ScheduleDTO>>>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Map<String, List<ScheduleDTO>>>?, response: Response<Map<String, List<ScheduleDTO>>>?) {

                scheduledto = response!!.body();

                valueArray!!.clear() //클리어..
                keyArray = ArrayList(scheduledto!!.keys) //키값
                for ((index,date) in keyArray!!.withIndex()) { //  withindex를 쓰면 index값까지 불러드린다
                    //헤더만듬
                    var scheduleDTO = ScheduleDTO(null, null, null, null, null,null, keyArray!![index]!!, null, null, null)
                    valueArray!!.add(scheduleDTO) //head연결 (date)

                    //tail연결 (content)
                    for (value in scheduledto!![date]!!) {
                        //println(value.ds_name)
                        valueArray!!.add(value)
                    }
                }
                notifyDataSetChanged()
                println(valueArray.toString())
            }

        })
    }



}