package com.jhsoft.woghl.coin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jhsoft.woghl.coin.button2.Button2Fragment
import com.jhsoft.woghl.coin.button3.Button3Fragment
import com.jhsoft.woghl.coin.button4.Button4Fragment
import com.jhsoft.woghl.coin.schedule.ScheduleRecyclerViewFragment
import com.jhsoft.woghl.coin.utl.VersionChecker
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        main_button1.setImageResource(R.drawable.icon_1_on)

        VersionChecker(this)

        var ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.framelayout_main, ScheduleRecyclerViewFragment())
        ft.commit()

        main_button1.setOnClickListener {
            main_button1.setImageResource(R.drawable.icon_1_on)
            main_button2.setImageResource(R.drawable.icon_2_off)
            main_button3.setImageResource(R.drawable.icon_3_off)
            main_button4.setImageResource(R.drawable.icon_4_off)
            var ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.framelayout_main, ScheduleRecyclerViewFragment())
            ft.commit()
        }

        main_button2.setOnClickListener {
            main_button1.setImageResource(R.drawable.icon_1_off)
            main_button2.setImageResource(R.drawable.icon_2_on)
            main_button3.setImageResource(R.drawable.icon_3_off)
            main_button4.setImageResource(R.drawable.icon_4_off)
            var ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.framelayout_main, Button2Fragment())
            ft.commit()
        }

        main_button3.setOnClickListener {
            main_button1.setImageResource(R.drawable.icon_1_off)
            main_button2.setImageResource(R.drawable.icon_2_off)
            main_button3.setImageResource(R.drawable.icon_3_on)
            main_button4.setImageResource(R.drawable.icon_4_off)
            var ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.framelayout_main, Button3Fragment())
            ft.commit()
        }

        main_button4.setOnClickListener {
            main_button1.setImageResource(R.drawable.icon_1_off)
            main_button2.setImageResource(R.drawable.icon_2_off)
            main_button3.setImageResource(R.drawable.icon_3_off)
            main_button4.setImageResource(R.drawable.icon_4_on)
            var ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.framelayout_main, Button4Fragment())
            ft.commit()
        }
    }




}
