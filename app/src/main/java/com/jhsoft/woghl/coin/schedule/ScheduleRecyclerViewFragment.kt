package com.jhsoft.woghl.coin.schedule

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.jhsoft.woghl.coin.R

/**
 * Created by woghl on 2018-01-28.
 */


class ScheduleRecyclerViewFragment : Fragment(){
    var recyclerview:RecyclerView?=null
    var scheduleAdapter = ScheduleAdapter()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater!!.inflate(R.layout.schedule_recyclerview_fragment, container, false);

        recyclerview = view.findViewById<RecyclerView>(R.id.schdulerecyclerview)
        recyclerview!!.adapter = scheduleAdapter
        recyclerview!!.layoutManager = LinearLayoutManager(activity)
        var edit_search = view.findViewById<EditText>(R.id.coin_search)
        edit_search.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                var recyclerview = view.findViewById<RecyclerView>(R.id.schdulerecyclerview)
                recyclerview.adapter = ScheduleAdapter(p0.toString())
                recyclerview.layoutManager = LinearLayoutManager(inflater.context)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })


        /*coin_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //수정하기 전 문자열
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {} //수정한 후 문자열

            override fun afterTextChanged(editable: Editable) {

            }
        })*/


        return view;
    }

    override fun onResume() {
        super.onResume()
       scheduleAdapter.test()
    }
}
