package com.jhsoft.woghl.coin.model

/**
 * Created by Lim on 2017-06-17.
 */

data class G5_BoardDTO (
    var wr_subject: String? = null,
    var wr_content: String? = null,
    var wr_link: String? = null,
    var wr_datetime: String? = null
)
